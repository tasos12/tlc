package com.monk.tlc;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Stage implements Parcelable {
    private int stageImg;
    private String stageRules, stageName;
    private StageTask[] stageTasks;

    public Stage(String name, int img, String rules, StageTask... tasks) {
        stageName = name;
        stageImg = img;
        stageRules = rules;
        stageTasks = tasks;
    }

    /**
     * This will be used only by the MyCreator
     *
     * @param in
     */
    public Stage(Parcel in) {
        stageName = in.readString();
        stageImg = in.readInt();
        stageRules = in.readString();
        stageTasks = (StageTask[]) in.createTypedArray(StageTask.CREATOR);
    }

    public int getImg() {
        return stageImg;
    }

    public String getRules() {
        return stageRules;
    }

    public String getName() {
        return stageName;
    }

    public StageTask[] getTasks() {
        return stageTasks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stageName);
        dest.writeInt(stageImg);
        dest.writeString(stageRules);
        dest.writeParcelableArray(stageTasks, 0);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Stage createFromParcel(Parcel in) {
            return new Stage(in);
        }

        public Stage[] newArray(int size) {
            return new Stage[size];
        }
    };
}
