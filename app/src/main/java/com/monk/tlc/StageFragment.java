package com.monk.tlc;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class StageFragment extends Fragment {

    private final String TAG = "TLC StageFragment";

    private OnStageFragmentInteractionListener mListener;
    private ListView mStageListView;
    private ArrayList<Stage> mStageData;
    private ArrayList<String> mStageNames;
    private ArrayAdapter<String> mStageAdapter;


    public StageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mStageNames = new ArrayList<String>();
        mStageData = new ArrayList<Stage>();

        mStageData.add(new Stage("Stage 1", R.drawable.stage, "Description & rules",
                                new StageTask("Task 1", 0, 5, 10),
                                new StageTask("Task 2", 0, -10)));
        mStageNames.add(mStageData.get(0).getName());

        mStageAdapter = new ArrayAdapter<>(getContext(), R.layout.stage_item, R.id.textView, mStageNames);

    }

    @Override
    public void onStart() {
        super.onStart();
        mStageListView = (ListView) getView().findViewById(R.id.stageList);
        mStageListView.setAdapter(mStageAdapter);
        mStageAdapter.notifyDataSetChanged();
        mStageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), StageActivity.class);
                Bundle data = new Bundle();
                data.putParcelable("Stage", mStageData.get(position));
                intent.putExtra("data", data);
                startActivity(intent);
                //TODO:Change static object with database download
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stage, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onStageFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnStageFragmentInteractionListener) {
            mListener = (OnStageFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnStageFragmentInteractionListener {
        void onStageFragmentInteraction(Uri uri);
    }
}
