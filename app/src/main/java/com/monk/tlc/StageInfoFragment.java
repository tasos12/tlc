package com.monk.tlc;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class StageInfoFragment extends Fragment {

    private static final String TAG = "StageInfoFragment";
    private static final String STAGE_PARAM = "stage";

    private OnStageInfoFragmentInteractionListener mListener;
    private Stage mStage;
    private ImageView mStageImageView;
    private TextView mRulesTextView;

    public StageInfoFragment() {
        // Required empty public constructor
    }

    public static StageInfoFragment newInstance(Stage stage) {
        StageInfoFragment fragment = new StageInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(STAGE_PARAM, stage);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStage = (Stage) getArguments().get(STAGE_PARAM);
        }
        else Log.d(TAG, "onCreate: arguments are null");
    }

    @Override
    public void onStart() {
        super.onStart();
        mStageImageView = getView().findViewById(R.id.stageImageView);
        mRulesTextView = getView().findViewById(R.id.rulesTxtView);

        if(mStage == null) Log.d(TAG, "onStart: mStage is null");

        mStageImageView.setImageResource(R.drawable.stage);
        mRulesTextView.setText(mStage.getRules());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stage_info, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onStageInfoFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnStageInfoFragmentInteractionListener) {
            mListener = (OnStageInfoFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnStageInfoFragmentInteractionListener {
        void onStageInfoFragmentInteraction(Uri uri);
    }
}
