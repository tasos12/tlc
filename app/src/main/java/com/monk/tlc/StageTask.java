package com.monk.tlc;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class StageTask implements Parcelable {

    private final static String TAG = "StageTask";

    private int[] levels;
    private String label;

    public StageTask(String label, int... levels) {
        //Log.d(TAG, "Number of task levels" + String.valueOf(levels.length));
        this.label = label;
        this.levels = levels;
    }

    public StageTask(Parcel in) {
        label = in.readString();
        levels = in.createIntArray();
    }

    public int[] getLevels() {
        return levels;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeIntArray(levels);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public StageTask createFromParcel(Parcel in) {
            return new StageTask(in);
        }

        public StageTask[] newArray(int size) {
            return new StageTask[size];
        }
    };
}
