package com.monk.tlc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class TableAdapter extends ArrayAdapter<TableDataModel> {

    private List<TableDataModel> dataSet;
    private final LayoutInflater inflater;
    private final int layoutResource;
    Context mContext;

    public TableAdapter(Context context, int resource, List<TableDataModel> data) {
        super(context, R.layout.table_item, data);
        this.dataSet = data;
        this.mContext=context;
        inflater = LayoutInflater.from(context);
        layoutResource = resource;
    }

    public void setData(@NonNull List<TableDataModel> data) {
        dataSet = data;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        TableDataModel teamData = dataSet.get(position);
        holder.txtName.setText(teamData.getName());
        holder.txtCategory.setText(teamData.getCategory());
        holder.txtScore.setText(teamData.getScore());

        return convertView;
    }


    // View lookup cache
    private static class ViewHolder {
        public TextView txtName;
        public TextView txtCategory;
        public TextView txtScore;

        public ViewHolder(View itemView) {
            txtName = itemView.findViewById(R.id.teamName);
            txtCategory = itemView.findViewById(R.id.teamCategory);
            txtScore = itemView.findViewById(R.id.teamScore);
        }
    }
}
