package com.monk.tlc;

public class TableDataModel {

    private String teamName, teamCategory, teamScore;

    public TableDataModel(String name, String category, String score) {
        teamName = name;
        teamCategory = category;
        teamScore = score;
    }

    public String getName() {
        return teamName;
    }

    public String getCategory() {
        return teamCategory;
    }

    public String getScore() {
        return teamScore;
    }
}
