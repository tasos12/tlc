package com.monk.tlc;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class TableFragment extends Fragment {

    private OnTableFragmentInteractionListener mListener;
    private ListView tableList;
    private ArrayList tableData;
    private TableAdapter tableAdapter;

    public TableFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO: Create Buttons for sorting by name, category or score.

        tableData = new ArrayList<TableDataModel>();

        //testing with arrayList
        tableData.add(new TableDataModel("Robots", getResources().getString(R.string.primary), "120"));
        tableData.add(new TableDataModel("Yikes", getResources().getString(R.string.secondary), "170"));
        tableData.add(new TableDataModel("Nope", getResources().getString(R.string.highschool), "300"));

        tableAdapter = new TableAdapter(getContext(), R.layout.table_item, tableData);
    }

    @Override
    public void onStart() {
        super.onStart();
        tableList = (ListView) getView().findViewById(R.id.tableList);
        tableList.setAdapter(tableAdapter);
        tableAdapter.setData(tableData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_table, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onTableFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTableFragmentInteractionListener) {
            mListener = (OnTableFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnTableFragmentInteractionListener {
        void onTableFragmentInteraction(Uri uri);
    }
}
